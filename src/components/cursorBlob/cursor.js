import React from "react";

import "./styles.scss";

class CursorBlob extends React.Component {
  state = { x: 0, y: 0 };

  onMouseMove = e => {
    this.setState({ x: e.clientX, y: e.clientY });
  };

  render() {
    const { x, y } = this.state;
    return (
      <div id="cursorArea" onMouseMove={this.onMouseMove}>
        <div id="cursorBlob" style={{top: `${y}px`, left: `${x}px`}}></div>
        {/* <div id="cursorBlob" style={{transform: `translate(${x}px, ${y}px)`}}></div> */}

        <div>{x}, {y}</div>
      </div>
    );
  }
}

export default CursorBlob;

// articles that helped
// https://stackoverflow.com/questions/42182481/getting-mouse-coordinates-in-react-and-jquery
// https://codepen.io/droopyeyelids/pen/GRJvObb
// https://neverbland.com/work