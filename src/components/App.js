import React from "react";

import CursorBlob from "./cursor/cursor";

const App = () => {
  return (
    <div>
      <CursorBlob />
      App is alive
    </div>
  );
};

export default App;
